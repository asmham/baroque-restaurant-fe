$(function() {
    $(".test").click(function() {
        $(".show_gallery").fadeIn();
        let index_swip = $(this).data("index");
        var thumbs = new Swiper('.gallery-thumbs', {
            slidesPerView: 'auto',
            spaceBetween: 10,
            centeredSlides: true,
            loop: true,
            slideToClickedSlide: true,
            initialSlide: index_swip,
        });
        var slider = new Swiper('.gallery-slider', {
            slidesPerView: 1,
            centeredSlides: true,
            loop: true,
            loopedSlides: 6,
            initialSlide: index_swip,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
        slider.controller.control = thumbs;
        thumbs.controller.control = slider;
    });
    $(".close_gal, .overlay").click(function() {
        $(".show_gallery").fadeOut();
    });
});