$(document).ready(function() {
    // if HTML DOM Element that contains the map is found...
    if (document.getElementById('map_canvas')) {

        var latLng = new google.maps.LatLng(46.315367036782526, 7.462418497893209),
            markerIcon = {
                url: './img/maps.png',
                scaledSize: new google.maps.Size(60, 60),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(20, 40)
            };


        var mapOptions = {
            zoom: 18,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);


        var marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: markerIcon
        });



    }
});