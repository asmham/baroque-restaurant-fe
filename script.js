function scrollFooter(scrollY, heightFooter) {
  if (scrollY >= heightFooter) {
    $("footer").css({
      bottom: "0px",
    });
  } else {
    $("footer").css({
      bottom: "-" + heightFooter + "px",
    });
  }
}

footHt = $("footer").height();
$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};
if ($(".section_hold").length) {
  $(window).on("resize scroll", function () {
    if ($(".section_hold").isInViewport()) {
      $(".section_hold .bg_sec_fix").css({
        height: "100vh",
      });
      $(".footer").css({
        bottom: "-" + footHt + "px",
      });
    } else {
      $(".section_hold .bg_sec_fix").css({
        height: "0px",
      });
      $(".footer").css({
        bottom: "0px",
      });
    }
  });
}
$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll > 200) {
    $(".bg-video-cover").addClass("scale-bg");
  } else {
    $(".bg-video-cover").removeClass("scale-bg");
  }
});

function scrollHandler() {
  if ($(".cover_page").length) {
    var windowHeight = $(window).height();
    var heightDocument =
      windowHeight + $(".content").height() + $("footer").height() - 20;
    $("#scroll-animate, #scroll-animate-main").css({
      height: heightDocument + "px",
    });
    $(".cover_page").css({
      height: windowHeight + "px",
      "line-height": windowHeight + "px",
    });

    var scroll = window.scrollY;
    $("#scroll-animate-main").css({
      top: "-" + scroll + "px",
    });
    $(".cover_page").css({
      "background-position-y": 50 - (scroll * 100) / heightDocument + "%",
    });
    $(".bg_hold").css({
      "background-position-y": 50 - (scroll * 100) / heightDocument + "%",
    });
  } else {
    heightDocument = $(".content").height() + $("footer").height() - 20;
    $("#scroll-animate, #scroll-animate-main").css({
      height: heightDocument + "px",
    });

    var scroll = window.scrollY;
    $("#scroll-animate-main").css({
      top: "-" + scroll + "px",
    });
  }
}

$(document).ready(function () {
  scrollHandler();
  window.addEventListener("scroll", scrollHandler);

  function hoverContent(value) {
    $("#list-content li").removeClass("show");
    $("#list-content li:nth-child(" + value + ")").addClass("show");
  }
  var liBg = $(".list__item:first a").attr("data-bg");
  $(".changebg").css("background-image", "url(" + liBg + ")");
  $(".list__item a").mouseenter(function () {
    var thisLiBg = $(this).attr("data-bg");

    $(".changebg").css({
      "background-image": "url(" + thisLiBg + ")",
      transition: "1s ease-in-out",
    });
  });
  $(".w_links li").hover(function () {
    var value = $(this).index() + 1;
    hoverContent(value);
  });
  $(".w_links .list__item a").click(function (e) {
    e.preventDefault();
  });
});

$(window).on("load resize orientationchange", scrollHandler);

$(document).ready(function () {
  $(window)
    .resize(function () {
      $("#scroll-animate, #scroll-animate-main").css({
        height: $(".content").outerHeight() + $(".footer").outerHeight(),
      });
    })
    .trigger("resize");
});
(split_cover = new SplitType(document.querySelectorAll(".split_cover"), {
  split_cover: "chart,words, lines",
})),
  (chars_cover = split_cover.chars);
let t1 = gsap.timeline();

t1.staggerFromTo(
  ".menu_head ul li a",
  0.45,
  {
    yPercent: 100,
    opacity: 0,
    ease: "power1.out",
  },
  {
    yPercent: 0,
    opacity: 1,
    ease: "power1.out",
  },
  0.15,
  2.5
);

t1.from(
  ".menu_cover li a",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1.3,
    ease: "power1.out",
  },
  2.5
);

t1.from(
  ".btn_menu",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1.1,
    ease: "power1.out",
  },
  3.3
);
t1.from(
  ".lab_cover",
  {
    yPercent: 50,
    opacity: 0,
    duration: 0.5,
    ease: "power2.out",
  },
  3.7
);
t1.from(
  chars_cover,
  {
    duration: 0.5,
    opacity: 0,
    scale: 0,
    skewY: 100,
    ease: "power2.out",
    stagger: 0.07,
  },
  3.5
);

t1.from(
  ".turb2",
  {
    duration: 0.5,
    opacity: 0,
    rotate: 4,
    ease: "power1.out",
  },
  6.6
);
t1.from(
  ".turb3",
  {
    duration: 0.8,
    opacity: 0,
    rotate: 1,
    ease: "power1.out",
  },
  6.6
);
t1.from(
  ".turb4",
  {
    duration: 0.8,
    opacity: 0,
    rotate: 6,
    ease: "power1.out",
  },
  6.6
);
t1.from(
  ".txt_btn",
  {
    yPercent: 50,
    opacity: 0,
    duration: 0.7,
    ease: "power2.out",
  },
  5.5
);

t1.from(
  ".play_cover",
  {
    duration: 0.8,
    opacity: 0,
    duration: 1.1,
    ease: "power1.out",
  },
  6.5
);

t1.from(
  ".right_b_cover",
  {
    yPercent: 100,
    opacity: 0,
    duration: 0.7,
    ease: "power2.out",
  },
  7.6
);

t1.from(
  ".line_cover",
  {
    yPercent: 100,
    opacity: 0,
    duration: 0.7,
    ease: "power2.out",
  },
  8
);

t1.from(
  ".content_vertical_txt",
  {
    yPercent: 100,
    opacity: 0,
    duration: 0.7,
    ease: "power1.out",
  },
  8
);

t1.from(
  ".line_cover2",
  {
    yPercent: -100,
    opacity: 0,
    duration: 2,
    // yoyo: true,
    repeat: -1,
    repeatDelay: 0.8,
    ease: "power1.out",
    stagger: 0.3,
  },
  8.5
);

t1.from(
  ".item",
  {
    xPercent: 100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
    stagger: {
      amount: 0.5,
      from: "left",
    },
  },
  4
);

t1.from(
  ".header-img",
  {
    xPercent: -100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
  },
  4.1
);

t1.from(
  ".heading1",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
  },
  1.2
);
t1.from(
  ".heading2",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
  },
  1.3
);
t1.from(
  ".heading3",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
  },
  1.4
);
t1.from(
  ".heading4",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1,
    ease: "power1.out",
  },
  1.5
);

t1.from(
  ".sub-heading",
  {
    yPercent: 100,
    opacity: 0,
    duration: 1.2,
    ease: "power1.out",
  },
  3.5
);

t1.from(
  ".title_cover_mobile",
  {
    xPercent: 100,
    opacity: 0,
    duration: 1.1,
    ease: "power1.out",
  },
  4.5
);
t1.from(
  ".description_mobile",
  {
    xPercent: 100,
    opacity: 0,
    duration: 1.1,
    ease: "power1.out",
  },
  4.7
);
gsap.to(".pContent", {
  yPercent: -20,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});

gsap.to(".pImage", {
  yPercent: -30,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});

gsap.from(".cercle1", {
  yPercent: 20,
  opacity: 10,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});
gsap.to(".cercle1", {
  yPercent: 0,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});

gsap.from(".cercle2", {
  yPercent: 20,
  opacity: 1,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});
gsap.to(".cercle2", {
  yPercent: 0,
  ease: "power1.out",
  scrollTrigger: {
    trigger: ".pSection",
    scrub: true,
  },
});
gsap.to(
  ".bg_sec",
  {
    scale: 1,
    delay: 0.7,
    opacity: 1,
    ease: "power1.out",
    duration: 1.3,
    scrollTrigger: {
      trigger: ".container_flexgrow",
      scrub: false,
    },
  },
  1
);
gsap.registerPlugin(ScrollTrigger);

let tl2 = gsap.timeline();
tl2.to(".scrollingText", {
  x: -4000,
  duration: 50,
  repeat: -1,
  ease: "linear",
});
let tl = gsap.timeline().reverse();
tl.to(".scrollingText", {
  xPercent: 15,
  scrollTrigger: {
    trigger: ".scrollingText",
    scrub: 1,
  },
});

$(document).ready(function () {
  var headr = $(".globNav");
  var customMenu = $(".custom-menu");
  var bd = $("body");
  var tlsplit = gsap.timeline(),
    split = new SplitType(document.querySelectorAll(".split"), {
      split: "chart,words, lines",
    }),
    chars = split.chars;

  var tlsplit2 = gsap.timeline();
  var tlsplitright2 = gsap.timeline();
  var tlup = gsap.timeline();
  var tlup2 = gsap.timeline();
  var tlsplitright = gsap.timeline(),
    split_r = new SplitType(
      document.querySelectorAll(".right_linknav .split"),
      {
        split_r: "chart,words, lines",
      }
    ),
    chars_r = split_r.chars;

  gsap.set(".split", { perspective: 400 });

  tlsplit.from(
    chars,
    {
      duration: 0.8,
      opacity: 0,
      scale: 0,
      skewY: 100,
      ease: "back",
      stagger: 0.02,
    },
    "+0.9"
  );
  tlsplitright.from(
    chars_r,
    {
      duration: 0.8,
      opacity: 0,
      scale: 0,
      skewY: 100,
      ease: "back",
      stagger: 0.02,
    },
    1.1
  );
  tlsplit2.from(chars, {
    duration: 2,
    opacity: 1,
    ease: "back",
  });
  tlsplitright2.from(chars_r, {
    duration: 2,
    opacity: 1,
    ease: "back",
  });

  tlup.from(
    ".detail1",
    {
      yPercent: 50,
      opacity: 0,
      duration: 1,
      ease: "power1.out",
    },
    2.5
  );
  tlup.from(
    ".detail2",
    {
      yPercent: 50,
      opacity: 0,
      duration: 1,
      ease: "power1.out",
    },
    2.8
  );

  tlup.from(
    ".custom-menu .end_footer ",
    {
      yPercent: 50,
      opacity: 0,
      duration: 1,
      ease: "power1.out",
    },
    3
  );
  tlup2.from(".little_menu_nav ", {
    duration: 2,
    opacity: 1,
    ease: "back",
  });

  if (customMenu.length) {
    var btnMenu = $("*[toggle-menu]");
    // console.log('custom menu active');
    var duration = 1;
    var duration_menu = 2;
    var menuImage = $(".menu-image img");
    var svgcover = $(".overlay_line");
    var buttomcover = $(".row_b_cover");
    var txtcover = $(".big_txt_cover");
    var menuOverlayImage = $(".menu-image .overlay-image");
    var menuLinks = $(".custom-menu ul li .links_menu");
    var menuLinksHeight = menuLinks.outerHeight();
    var otherlink_menu = $(".menu_head ul li.link_head");

    var menuToggle = new TimelineMax({ pause: true });
    // menuToggle.set(menuLinks, { y: menuLinksHeight });
    menuToggle.to(customMenu, 0.5, {
      opacity: 1,
      height: "100vh",
      ease: "expo.inOut",
    });
    menuToggle.to(menuOverlayImage, {
      height: 0,
      ease: "expo.inOut",
    });
    menuToggle.to(
      menuImage,
      {
        scale: 1,
        ease: "expo.inOut",
      },
      "-=" + duration
    );
    menuToggle.staggerTo(
      menuLinks,
      duration,
      {
        // y: 0,
        opacity: 1,
        ease: "expo.inOut",
      },
      0.06,
      "-=" + duration
    );

    menuToggle.reverse();

    function toggleMenu() {
      menuToggle.reversed(!menuToggle.reversed());
    }
    btnMenu.on("click", function () {
      !$(this).hasClass("active") && tlsplit.restart();
      $(this).hasClass("active") && tlsplit2.restart();
      !$(this).hasClass("active") && tlsplitright.restart();
      $(this).hasClass("active") && tlsplitright2.restart();
      !$(this).hasClass("active") && tlup.restart();
      $(this).hasClass("active") && tlup2.restart();
      toggleMenu();
      btnMenu.toggleClass("active");
      customMenu.toggleClass("active");
      bd.toggleClass("noscroll");
      headr.toggleClass("black_nav");
      svgcover.toggleClass("z_m");
      buttomcover.toggleClass("z_m");
      txtcover.toggleClass("z_m");
      otherlink_menu.toggleClass("fadeout_l");
    });
  }
});

$(document).ready(function () {
  $(".btn_menu").click(function () {
    $(".icon").toggleClass("active");
  });
  $(".back_top").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
  });

  // var lastScrollTop = 0;
  // $(window).scroll(function(event) {
  //     var st = $(this).scrollTop();
  //     if (st > lastScrollTop) {
  //         $(".globNav").addClass("hide_menu");
  //     } else {
  //         $(".globNav").removeClass("hide_menu");
  //     }
  //     lastScrollTop = st;
  // });

  // $(window).bind('scroll', function() {
  //     if ($(window).scrollTop() > 650) {
  //         $('.globNav').hide();
  //     } else {
  //     }
  // });
});

$("a.scroll_mouse-link").click(function (e) {
  e.preventDefault();
  $id = $(this).attr("data-href");
  $("body,html").animate(
    {
      scrollTop: $($id).offset().top - 20,
    },
    750
  );
});

$.fn.parallax = function (resistance, mouse) {
  $el = $(this);
  TweenLite.to($el, 0.2, {
    x: -((mouse.clientX - window.innerWidth / 2) / resistance),
    y: -((mouse.clientY - window.innerHeight / 2) / resistance),
  });
};

$.fn.parallax2 = function (resistance, mouse) {
  $el = $(this);
  TweenLite.to($el, 0.4, {
    x: -((mouse.clientX - window.innerWidth / 2) / resistance),
    y: -((mouse.clientY - window.innerHeight / 2) / resistance),
  });
};

$(document).mousemove(function (e) {
  $(".left_cover").parallax(-30, e);
  $(".right_cover").parallax2(-20, e);
});
$(function () {
  $(window).on("scroll", function () {
    if ($(window).scrollTop() > 50) {
      $(".globNav").addClass("active_header");
    } else {
      //remove the background property so it comes transparent again (defined in your css)
      $(".globNav").removeClass("active_header");
    }
  });
});
$(".video")
  .parent()
  .click(function () {
    if ($(this).children(".video").get(0).paused) {
      $(this).children(".video").get(0).play();
      $(this).children(".playpause").fadeOut();
    } else {
      $(this).children(".video").get(0).pause();
      $(this).children(".playpause").fadeIn();
    }
  });

$(document).ready(function () {
  $(".navigation_sectors .item_haschild").hover(
    function () {
      $(this).find(".sub_menu_sector").slideDown("50");
      $(this).find(".icon_nav").toggleClass("collapsed");
    },
    function () {
      $(this).find(".sub_menu_sector").slideUp("50");
      $(this).find(".icon_nav").toggleClass("collapsed");
    }
  );
});
$(".submenu_item").hover(function () {
  $this = $(this);
  $(".change_bgsector").css(
    "background-image",
    "url(" + $(this).data("bg") + ")"
  );
});
$(".submenu_item").mouseleave(function () {
  $(".change_bgsector").css("background-image", "none");
});

document.addEventListener("DOMContentLoaded", function () {
  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0.4,
  };

  // IMAGE ANIMATION

  let revealCallback = (entries) => {
    entries.forEach((entry) => {
      let container = entry.target;

      if (entry.isIntersecting) {
        container.classList.add("animating");
        return;
      }

      if (entry.boundingClientRect.top > 0) {
        container.classList.remove("animating");
      }
    });
  };

  let revealObserver = new IntersectionObserver(revealCallback, options);

  document.querySelectorAll(".reveal").forEach((reveal) => {
    revealObserver.observe(reveal);
  });
});

document.addEventListener("DOMContentLoaded", function () {
  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0.4,
  };

  // IMAGE ANIMATION

  let revealCallback = (entries) => {
    entries.forEach((entry) => {
      let container = entry.target;

      if (entry.isIntersecting) {
        container.classList.add("animating_story");
        return;
      }

      if (entry.boundingClientRect.top > 0) {
        container.classList.remove("animating_story");
      }
    });
  };

  let revealObserver = new IntersectionObserver(revealCallback, options);

  document.querySelectorAll(".container_img_story").forEach((reveal) => {
    revealObserver.observe(reveal);
  });
});

jQuery.validator.setDefaults({
  errorPlacement: function (error, element) {
    if (element.parent(".form-group").length) {
      error.insertAfter(element.parent());
    } else if (element.parent(".radio-inline").length) {
      error.insertAfter(element.parent().parent());
    } else {
      error.insertAfter(element);
    }
  },
  errorElement: "div",
  errorClass: "help-block",
  //ignore: ''
});

$(".newsletter_form").validate();

$(".contact_forms").validate();

$(".row_form .input_news")
  .on("focus blur", function (e) {
    $(this)
      .parents(".row_form")
      .toggleClass("is-focused", e.type === "focus" || this.value.length > 0);
  })
  .trigger("blur");

$(function () {
  $(".lazy").lazy();
  var counter = 0;
  var c = 0;
  var i = setInterval(function () {
    $(".count_load").html(c);

    counter++;
    c++;

    if (counter == 101) {
      clearInterval(i);
    }
  }, 10);
});
$(".team_carousel").owlCarousel({
  margin: 1,
  loop: false,
  dots: true,
  nav: true,
  items: 3,
  // itemsDesktop: [1199, 3],
  // itemsDesktopSmall: [980, 2],
  // itemsMobile: [600, 1],
  navigation: true,
  navigationText: ["", ""],
  pagination: true,
  navText: [
    '<span><svg width="36" height="32" viewBox="0 0 36 32" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1.03685 15.1568L0.415822 15.8332L1.03685 16.5095L11.5876 28L13.0608 26.6473L3.97855 16.7561L36 16.7561L36 14.7561L4.11999 14.7561L13.0608 5.01902L11.5876 3.66633L1.03685 15.1568Z" fill="#141414"/></svg></span>',
    '<span><svg width="36" height="32" viewBox="0 0 36 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M34.9631 12.8432L35.5842 12.1668L34.9631 11.4905L24.4124 0L22.9392 1.35269L32.0214 11.2439H0V13.2439H31.88L22.9392 22.981L24.4124 24.3337L34.9631 12.8432Z" fill="#141414"/></svg></span>',
  ],
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
      stagePadding: 50,
    },
    600: {
      items: 3,

      stagePadding: 100,
    },
    1000: {
      items: 3,

      stagePadding: 150,
    },
    1500: {
      items: 3,
      stagePadding: 200,
    },
  },
});
// $(".gallery_sector").owlCarousel({
//     margin: 50,
//     loop: false,
//     dots: false,
//     nav: true,
//     items: 2,
//     navigation: true,
//     navigationText: ["", ""],
//     pagination: true,
//     navText: ['<span><svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><g><path d="M40.25 32L23.75 32" stroke="#D6A393"/><path d="M30.5 38.75L23.75 32L30.5 25.25" stroke="#D6A393"/><rect x="0.5" y="63.5" width="63" height="63" rx="31.5" transform="rotate(-90 0.5 63.5)" stroke="#D6A393"/></g></svg></span>', '<span><svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.75 32H40.25" stroke="#D6A393"/><path d="M33.5 25.25L40.25 32L33.5 38.75" stroke="#D6A393"/><rect x="63.5" y="0.5" width="63" height="63" rx="31.5" transform="rotate(90 63.5 0.5)" stroke="#D6A393"/></svg></span>'],
//     responsiveClass: true,
//     responsive: {
//         0: {
//             items: 1,
//             stagePadding: 16,
//             nav: true,
//             margin: 16
//         },
//         600: {
//             items: 3,
//             margin: 16,
//             stagePadding: 16,
//             nav: true,
//         },
//         980: {
//             items: 2,
//             loop: false,
//             nav: true,
//             margin: 80,
//             stagePadding: 50,
//         },
//         1500: {
//             items: 2,
//             loop: false,
//             stagePadding: 120,

//         }

//     }
// });

$(".instagram_carousel").owlCarousel({
  margin: 50,
  loop: false,
  dots: false,
  nav: true,
  items: 2,
  navigation: true,
  navigationText: ["", ""],
  pagination: true,
  navText: [
    '<span><svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><g><path d="M40.25 32L23.75 32" stroke="#D6A393"/><path d="M30.5 38.75L23.75 32L30.5 25.25" stroke="#D6A393"/><rect x="0.5" y="63.5" width="63" height="63" rx="31.5" transform="rotate(-90 0.5 63.5)" stroke="#D6A393"/></g></svg></span>',
    '<span><svg width="64" height="64" viewBox="0 0 64 64" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M23.75 32H40.25" stroke="#D6A393"/><path d="M33.5 25.25L40.25 32L33.5 38.75" stroke="#D6A393"/><rect x="63.5" y="0.5" width="63" height="63" rx="31.5" transform="rotate(90 63.5 0.5)" stroke="#D6A393"/></svg></span>',
  ],
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
      stagePadding: 16,
      nav: true,
      margin: 16,
    },
    600: {
      items: 3,
      margin: 16,
      stagePadding: 16,
      nav: true,
    },
    980: {
      items: 2,
      loop: false,
      nav: true,
      margin: 80,
      stagePadding: 120,
    },
    1500: {
      items: 2,
      loop: false,
      stagePadding: 120,
    },
  },
});
$(document).ready(function () {
  $(".loader-slide").delay(1500).fadeOut(1000);
  // $('.center').fadeOut(3000);
  setTimeout(function () {
    $("#loader").css("z-index", "0");
  }, 3000);
  setTimeout(function () {
    $(".bg-video-cover").addClass("imagescale1");
  }, 1000);
  setTimeout(function () {
    $(".turb1 path").addClass("path");
  }, 5000);
});

jQuery(document).ready(function (e) {
  //window.addEventListener("load", function() {

  let splitWords = function (selector) {
    var elements = document.querySelectorAll(selector);

    elements.forEach(function (el) {
      el.dataset.splitText = el.textContent;
      el.innerHTML = el.textContent
        .split(/\s/)
        .map(function (word) {
          return word
            .split("-")
            .map(function (word) {
              return '<span class="word">' + word + "</span>";
            })
            .join('<span class="hyphen">-</span>');
        })
        .join('<span class="whitespace"> </span>');
    });
  };

  let splitLines = function (selector) {
    var elements = document.querySelectorAll(selector);

    splitWords(selector);

    elements.forEach(function (el) {
      var lines = getLines(el);

      var wrappedLines = "";
      lines.forEach(function (wordsArr) {
        wrappedLines += '<div class="line_split"><div class="words">';
        wordsArr.forEach(function (word) {
          wrappedLines += word.outerHTML;
        });
        wrappedLines += "</div></div>";
      });
      el.innerHTML = wrappedLines;
    });
  };

  let getLines = function (el) {
    var lines = [];
    var line;
    var words = el.querySelectorAll("span");
    var lastTop;
    for (var i = 0; i < words.length; i++) {
      var word = words[i];
      if (word.offsetTop != lastTop) {
        // Don't start with whitespace
        if (!word.classList.contains("whitespace")) {
          lastTop = word.offsetTop;

          line = [];
          lines.push(line);
        }
      }
      line.push(word);
    }
    return lines;
  };

  splitLines(".anime_text");

  let revealTextup = document.querySelectorAll(".anime_text");

  gsap.registerPlugin(ScrollTrigger);
  let revealLinesup = revealTextup.forEach((element) => {
    const lines = element.querySelectorAll(".words");

    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: element,
        toggleActions: "restart none none reset",
      },
    });
    tl.set(element, { autoAlpha: 1 });
    tl.from(lines, 1, {
      yPercent: 100,
      ease: Power1.out,
      duration: 2,
      stagger: 0.25,
      delay: 0.1,
    });
  });

  (split_titles = new SplitType(document.querySelectorAll(".split_title"), {
    split_titles: "chart,words, lines",
  })),
    (chars_title = split_titles.chars);

  // let revealText = document.querySelectorAll(".split_title");

  let revealText = document.querySelectorAll(".section_about_home");

  gsap.registerPlugin(ScrollTrigger);
  let revealLines = revealText.forEach((element) => {
    const charst = element.querySelectorAll(".split_title .char");

    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: element,
        toggleActions: "restart none none reset",
      },
    });
    tl.set(element, { autoAlpha: 1 });
    tl.from(charst, 1, {
      duration: 0.1,
      opacity: 0,
      scale: 0,
      skewY: 100,
      ease: "power2.out",
      stagger: 0.07,
    });

    tl.from(
      "stars_anime",
      1,
      {
        duration: 0.8,
        opacity: 0,
        scale: 0,
        skewY: 100,
        ease: "power2.out",
        stagger: 0.07,
      },
      0.8
    );

    tl.from(
      ".txt_p p",
      {
        yPercent: 20,
        duration: 0.6,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.07,
      },
      1
    );
    tl.to(
      ".txt_p p",
      {
        yPercent: 0,
        duration: 0.6,
        opacity: 0.7,
        ease: "power2.out",
        stagger: 0.07,
      },
      1
    );

    tl.from(
      ".logo_brand",
      {
        yPercent: 20,
        duration: 0.6,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.07,
      },
      1.5
    );
    tl.from(
      ".center_black_btn .gold_btn",
      {
        yPercent: 20,
        duration: 0.5,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.07,
      },
      2
    );

    tl.from(
      ".img_about",
      {
        duration: 1,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.07,
      },
      3.4
    );

    let tl2 = gsap.timeline({
      scrollTrigger: {
        trigger: element,
        toggleActions: "restart none none reset",
      },
    });

    tl2.from(
      ".description_about p",
      {
        yPercent: 20,
        duration: 0.6,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.07,
      },
      1
    );
    tl2.to(
      ".description_about p",
      {
        yPercent: 0,
        duration: 0.6,
        opacity: 0.7,
        ease: "power2.out",
        stagger: 0.07,
      },
      1
    );
    tl2.from(
      ".content_concept p",
      {
        yPercent: 20,
        duration: 0.6,
        opacity: 0,
        ease: "power2.out",
        stagger: 0.08,
      },
      2
    );
    tl2.to(
      ".content_concept p",
      {
        yPercent: 0,
        duration: 0.6,
        opacity: 0.7,
        ease: "power2.out",
        stagger: 0.08,
      },
      2
    );
  });
  //});

  const boxes = gsap.utils.toArray(".fade_up");

  boxes.forEach((box, i) => {
    ScrollTrigger.create({
      trigger: box,
      toggleClass: "active",
      // this toggles the class again when you scroll back up:
      toggleActions: "play none none none",
      // this removes the class when the scrolltrigger is passed:
      // once: true,
    });
  });
});

jQuery(document).ready(function (e) {
  var WindowHeight = jQuery(window).height();
  var load_element = 0;
  var body_sc = $(".sectors");
  if (body_sc.length) {
    var scroll_position = jQuery(".sectors").offset().top;
    var screen_height = jQuery(window).height();
    var activation_offset = 0;
    var max_scroll_height = jQuery("body").height() + screen_height;
    var scroll_activation_point =
      scroll_position - screen_height * activation_offset;
    jQuery(window).on("scroll", function (e) {
      var y_scroll_pos = window.pageYOffset;
      var element_in_view = y_scroll_pos > scroll_activation_point;
      var has_reached_bottom_of_page =
        max_scroll_height <= y_scroll_pos && !element_in_view;

      if (element_in_view || has_reached_bottom_of_page) {
        jQuery(".sectors .progress .progress-value").addClass("animate");
      } else {
        jQuery(".sectors .progress .progress-value").removeClass("animate");
      }
    });
  }
});

jQuery(document).ready(function (e) {
  var WindowHeight = jQuery(window).height();
  var load_element = 0;
  var body_sc = $(".section_hold");
  if (body_sc.length) {
    var scroll_position = jQuery(".section_hold").offset().top;
    var screen_height = jQuery(window).height();
    var activation_offset = 0;
    var max_scroll_height = jQuery("body").height() + screen_height;
    var scroll_activation_point =
      scroll_position - screen_height * activation_offset;
    jQuery(window).on("scroll", function (e) {
      var y_scroll_pos = window.pageYOffset;
      var element_in_view = y_scroll_pos > scroll_activation_point;
      var has_reached_bottom_of_page =
        max_scroll_height <= y_scroll_pos && !element_in_view;

      if (element_in_view || has_reached_bottom_of_page) {
        jQuery(".bg_sec_fix").addClass("scale-inv");
      } else {
        jQuery(".bg_sec_fix").removeClass("scale-inv");
      }
    });
  }
});

$(document).ready(function () {
  var $button = $(".link_menu"),
    $page = $(".page");
  $button
    .on("mouseenter", function (e) {
      $(".bg_video-menu").addClass("opacity_1");
      var thisPageId = $(this).attr("id");

      gsap.to(thisPageId, 1, { autoAlpha: 1 });
      gsap.to($page.not(thisPageId), 0.5, { autoAlpha: 0 }); // faster
    })
    .mouseleave(function () {
      $(".bg_video-menu").removeClass("opacity_1");
    });
  const boxes = gsap.utils.toArray(".fadein_up");

  boxes.forEach((box, i) => {
    const anim = gsap.fromTo(
      box,
      { autoAlpha: 0, y: 50 },
      { duration: 2, autoAlpha: 1, y: 0 }
    );
    ScrollTrigger.create({
      trigger: box,
      animation: anim,
      toggleActions: "play none none none",
      once: true,
    });
  });
});

$(document).ready(function () {
  var $button = $(".event_link"),
    $page = $(".page_event");
  $button
    .on("mouseenter", function (e) {
      // console.log(e.currentTarget.id.slice(1))

      $(`#${e.currentTarget.id.slice(1)}`).addClass("clip_path");
      var thisPageId = $(this).attr("id");

      gsap.to(thisPageId, 1, { autoAlpha: 1 });
      gsap.to($page.not(thisPageId), 0.5, { autoAlpha: 0 }); // faster
    })
    .mouseleave(function () {
      $(".page_event").removeClass("clip_path");
    });
});
jQuery(document).ready(function (e) {
  (split_foo = new SplitType(document.querySelectorAll(".row_links_foo a"), {
    split_foo: "words, lines",
  })),
    (chars_title_foo = split_foo.lines);

  (split_foo2 = new SplitType(document.querySelectorAll(".foo_links li a  "), {
    split_foo2: "words, lines",
  })),
    (chars_title_foo2 = split_foo2.lines);

  const sectionBeforeFooter =
    document.querySelector(".content").lastElementChild;

  var tl_footer = gsap.timeline({
    scrollTrigger: {
      trigger: sectionBeforeFooter,
      start: `top  ${sectionBeforeFooter.clientHeight}px`,
      // end: "bottom 10px",
      toggleActions: "play none none reverse",
      onEnter: () => {
        $(".footer .progress_foo .progress-value").addClass("animate");
      },
    },
  });

  tl_footer.from(chars_title_foo, {
    y: "150%",
    transformOrigin: "0% 50% -50",
    duration: 1.5,
    ease: "power3.out",
    stagger: 0.05,
  });

  tl_footer.from(chars_title_foo2, {
    y: "150%",
    transformOrigin: "0% 50% -50",
    duration: 0.5,
    ease: "power1.out",
    stagger: 0.05,
  });

  tl_footer.from(".back_top", {
    opacity: 0,
    duration: 0.2,
    ease: "power1.out",
    stagger: 0.01,
  });
});

jQuery(document).ready(function (e) {
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 900) {
      $(".overlay_line ").addClass("z_m");
      $(".row_b_cover").addClass("z_m");
      $(".menu_cover").addClass("z_m");
      $(".after_header li a").addClass("z_m");
      $(".p_container .left").addClass("z_m");
      $(".left_block").addClass("z_m");
      $(".right_block").addClass("z_m");
    } else {
      $(".overlay_line ").removeClass("z_m");
      $(".row_b_cover").removeClass("z_m");
      $(".menu_cover").removeClass("z_m");
      $(".p_container .left").removeClass("z_m");
      $(".left_block").removeClass("z_m");
      $(".right_block").removeClass("z_m");
    }
  });
});
$(document).ready(function () {
  if ($(window).width() < 768) {
    $(".gallery_slide .post-img").removeClass("image-wrap");
  } else {
    $(".gallery_slide .post-img").addClass("image-wrap");
  }
});
$(document).ready(function () {
  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  };
});

$(document).ready(function () {
  if (document.cookie.indexOf("visited=true") == -1) {
    // load the overlay
    $(".modal_cookies").modal({
      show: true,
      ackdrop: "static",
      keyboard: false,
    });

    var year = 1000 * 60 * 60 * 24 * 365;
    var expires = new Date(new Date().valueOf() + year);
    document.cookie = "visited=true;expires=" + expires.toUTCString();
  }
});

$(document).ready(function () {
  // smooth scroll
  const SPEED_FACTOR = 0.1;
  const STOP_DISTANCE_PX = 1;

  let current = 0;
  let target = 0;
  let isMoving = false;
  let directionIsDownwards = false;
  const callbacks = [];
  const runCallbacks = function () {
    callbacks.forEach((cb) => cb(...arguments));
  };
  const animationFrame = () => {
    const target = window.scrollY || document.documentElement.scrollTop;
    const diff = Math.abs(current - target);
    if (diff >= STOP_DISTANCE_PX) {
      isMoving = true;
      directionIsDownwards = target > current;
      current += SPEED_FACTOR * (target - current);
    } else if (diff < STOP_DISTANCE_PX && isMoving) {
      isMoving = false;
      directionIsDownwards = target > current;
      current = target;
    }
    runCallbacks({ currentScroll: current, directionIsDownwards });
    requestAnimationFrame(animationFrame);
  };
  animationFrame();

  // smooth bg words

  const LOOPED_TEXTS_COUNT = 2; // choose any number from 2 to 100 so that total width of repeated texts is more than window width
  const IDLE_SPEED_PX_PER_FRAME = 1;
  // init
  const bgWordsElements = document.querySelectorAll("[data-bg-words]");
  bgWordsElements.forEach((bgWords) => {
    const text = bgWords.textContent;
    bgWords.textContent = "";
    bgWords.style.display = "flex";
    for (let i = 0; i < LOOPED_TEXTS_COUNT; i++) {
      const div = document.createElement("div");
      div.textContent = text;
      div.style.willChange = "transform";
      bgWords.appendChild(div);
    }
  });
  // hook up to smooth scroll's request animation frame
  // and introduce the time
  let time = 0;
  callbacks.push(({ currentScroll, directionIsDownwards }) => {
    if (directionIsDownwards) {
      time += 1;
    } else {
      time -= 1;
    }
    bgWordsElements.forEach((bgWords) => {
      const singleItemLength = bgWords.clientWidth / LOOPED_TEXTS_COUNT;
      if (time <= 0) {
        time += singleItemLength;
      }
      const position =
        (currentScroll + IDLE_SPEED_PX_PER_FRAME * time) % singleItemLength;
      for (let i = 0; i < LOOPED_TEXTS_COUNT; i++) {
        bgWords.children[i].style.transform = `translate(${position}px, 0)`;
      }
    });
  });
});
$(document).ready(function () {
  $(".nice-select").niceSelect();
  var headerHeight = $(".globNav ").outerHeight();
  let top_block = headerHeight;
  $(".section_detailnews").css("margin-top", top_block);
});

let elrotate = $(".rotate_me");
let play_cover = $(".play_cover");
timeliner = new TimelineMax({ paused: true });
timeliner.to(
  elrotate,
  {
    scale: 1.3,
  },
  0
);

play_cover.mouseenter(() => {
  timeliner.play();
});

play_cover.mouseleave(() => {
  timeliner.reverse();
});
// const turbulence1 = $("#feturb1");
// const turbulence2 = $("#feturb2");
// const turbulence3 = $("#feturb3");
// const turbulence4 = $("#feturb4");
// const durationTime = 12;

// $(".gold_btn").mouseenter((e) => {
//     const childerns = $(e.currentTarget).children();
//     const text1 = childerns[0];
//     const text2 = childerns[1];
//     const text3 = childerns[2];
//     const text4 = childerns[3];
//     gsap.to(text1, {
//         duration: 0,
//         delay: 0.1,
//         startAt: { css: { filter: "none" } },
//         css: { filter: "url(#distortion1)" },
//     });
//     gsap.to(text2, {
//         duration: 0.1,
//         delay: 0.11,
//         startAt: { css: { filter: "none" } },
//         css: { filter: "url(#distortion2)" },
//     });
//     gsap.to(text3, {
//         duration: 0.2,
//         delay: 0.12,
//         startAt: { css: { filter: "none" } },
//         css: { filter: "url(#distortion3)" },
//     });
//     gsap.to(text4, {
//         duration: 0.3,
//         delay: 0.13,
//         startAt: { css: { filter: "none" } },
//         css: { filter: "url(#distortion4)" },
//     });

//     gsap.to(turbulence1, {
//         duration: durationTime,
//         startAt: { attr: { baseFrequency: 0.02 } },
//         attr: { baseFrequency: 0.01 },
//     });
//     gsap.to(turbulence2, {
//         duration: durationTime,
//         startAt: { attr: { baseFrequency: 0.03 } },
//         attr: { baseFrequency: 0.02 },
//     });
//     gsap.to(turbulence3, {
//         duration: durationTime,
//         startAt: { attr: { baseFrequency: 0.04 } },
//         attr: { baseFrequency: 0.03 },
//     });
//     gsap.to(turbulence4, {
//         duration: durationTime,
//         startAt: { attr: { baseFrequency: 0.05 } },
//         attr: { baseFrequency: 0.04 },
//     });
// });

// $(".gold_btn").mouseleave((e) => {
//     setTimeout(() => {
//         const text1 = $(".turb1");
//         const text2 = $(".turb2");
//         const text3 = $(".turb3");
//         const text4 = $(".turb4");
//         gsap.to(text1, {
//             duration: 0,
//             startAt: { css: { filter: "url(#distortion1)" } },
//             css: { filter: "none" },
//         });
//         gsap.to(text2, {
//             duration: 0,
//             startAt: { css: { filter: "url(#distortion2)" } },
//             css: { filter: "none" },
//         });
//         gsap.to(text3, {
//             duration: 0,
//             startAt: { css: { filter: "url(#distortion3)" } },
//             css: { filter: "none" },
//         });
//         gsap.to(text4, {
//             duration: 0,
//             startAt: { css: { filter: "url(#distortion4)" } },
//             css: { filter: "none" },
//         });
//     }, 400);
// });

$(".section_grow").mouseenter((e) => {
  let time_flex = gsap.timeline();
  const childerns = $(e.currentTarget).children();
  const big_txt = childerns.find(".big_txt .line_split .words");
  const small_txt = childerns.find(".small_txt .line_split .words");
  const p_txt = childerns.find(" p .line_split .words");
  const number_card = childerns.find(".number_card .line_split .words");

  const btn_card = childerns.find(".col_btn");
  time_flex.fromTo(
    big_txt,
    {
      yPercent: 100,
      ease: Power1.out,
      duration: 0.8,
      stagger: 0.01,
      delay: 0.3,
      opacity: 0,
    },
    {
      opacity: 1,
      yPercent: 0,
    },
    0.5
  );

  time_flex.fromTo(
    small_txt,
    {
      opacity: 1,
      yPercent: 0,
    },
    {
      yPercent: 100,
      ease: Power1.out,
      duration: 0.8,
      stagger: 0.01,
      delay: 0.3,
      opacity: 0,
    },
    0.1
  );

  time_flex.fromTo(
    p_txt,
    {
      yPercent: 100,
      ease: Power1.out,
      duration: 0.8,
      stagger: 0.01,
      delay: 1.2,
      opacity: 0,
    },
    {
      delay: 0.6,
      opacity: 0.7,
      yPercent: 0,
    },
    0.2
  );
  time_flex.fromTo(
    number_card,
    {
      yPercent: 100,
      ease: Power1.out,
      duration: 0.8,
      stagger: 0.01,
      delay: 0.3,
      opacity: 0,
    },
    {
      opacity: 1,
      yPercent: 0,
    },
    0.8
  );
  time_flex.fromTo(
    btn_card,
    {
      ease: Power1.out,
      duration: 0.8,
      stagger: 0.01,
      delay: 0.3,
      opacity: 0,
    },
    {
      opacity: 1,
    },
    0.8
  );
  time_flex.play();
});

$(".section_grow").mouseleave((e) => {
  const childerns = $(e.currentTarget).children();
  const big_txt = childerns.find(".big_txt .line_split .words");
  const small_txt = childerns.find(".small_txt .line_split .words");
  const p_txt = childerns.find(" p .line_split .words");
  const number_card = childerns.find(".number_card .line_split .words");
  const btn_card = childerns.find(".col_btn");

  gsap.set(big_txt, { clearProps: "all", overwrite: true }, 1);

  gsap.set(small_txt, { clearProps: "all", overwrite: true }, 1);

  gsap.set(p_txt, { clearProps: "all", overwrite: true }, 1);

  gsap.set(number_card, { clearProps: "all", overwrite: true }, 1);

  gsap.set(btn_card, { clearProps: "all", overwrite: true }, 1);
});

$(document).ready(function () {
  var swiper = new Swiper(".swiper_gallery", {
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
    speed: 500,
    loop: false,
    // pagination: {
    //     el: ".swiper-pagination",
    //     type: "fraction"
    // },

    pagination: {
      el: ".swiper-pagination",
      type: "custom",
      clickable: false,
      renderCustom: function (swiper, current, total) {
        function numberAppend(d) {
          return d < 10 ? "0" + d.toString() : d.toString();
        }
        return `<span class="left_pag"> ${numberAppend(
          current
        )}</span>  <span class="right_pag">${numberAppend(total)}</span>`;
      },
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    on: {
      init: function () {
        $(".swiper-progress-bar").removeClass("animate_swip");
        $(".swiper-progress-bar").removeClass("active");
        $(".swiper-progress-bar").eq(0).addClass("animate_swip");
        $(".swiper-progress-bar").eq(0).addClass("active");
      },
      slideChangeTransitionStart: function () {
        $(".swiper-progress-bar").removeClass("animate_swip");
        $(".swiper-progress-bar").removeClass("active");
        $(".swiper-progress-bar").eq(0).addClass("active");
      },
      slideChangeTransitionEnd: function () {
        $(".swiper-progress-bar").eq(0).addClass("animate_swip");
      },
    },
  });

  var menu = [];
  jQuery(".swiper-slide").each(function (index) {
    menu.push(jQuery(this).find(".slide-inner").attr("data-text"));
  });
  var interleaveOffset = 0.5;
  var swiperOptions = {
    loop: true,
    speed: 1000,
    parallax: true,
    autoplay: {
      delay: 6500,
      disableOnInteraction: false,
    },
    watchSlidesProgress: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },

    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    on: {
      progress: function () {
        var swiper = this;
        for (var i = 0; i < swiper.slides.length; i++) {
          var slideProgress = swiper.slides[i].progress;
          var innerOffset = swiper.width * interleaveOffset;
          var innerTranslate = slideProgress * innerOffset;
          if (swiper.slides[i].querySelector(".slide-inner")) {
            swiper.slides[i].querySelector(".slide-inner").style.transform =
              "translate3d(" + innerTranslate + "px, 0, 0)";
          }
        }
      },

      touchStart: function () {
        var swiper = this;
        for (var i = 0; i < swiper.slides.length; i++) {
          swiper.slides[i].style.transition = "";
        }
      },

      setTransition: function (speed) {
        var swiper = this;
        for (var i = 0; i < swiper.slides.length; i++) {
          swiper.slides[i].style.transition = speed + "ms";
          if (swiper.slides[i].querySelector(".slide-inner")) {
            swiper.slides[i].querySelector(".slide-inner").style.transition =
              speed + "ms";
          }
        }
      },
    },
  };

  var swiper = new Swiper(".swiper_gallery", swiperOptions);

  var sliderBgSetting = $(".slide-bg-image");
  sliderBgSetting.each(function (indx) {
    if ($(this).attr("data-background")) {
      $(this).css(
        "background-image",
        "url(" + $(this).data("background") + ")"
      );
    }
  });
});
$(document).ready(function () {
  $(".contact_forms .form-input").focus(function () {
    $(this).parents(".form-group").addClass("focused");
  });

  $(".contact_forms .form-input").blur(function () {
    var inputValue = $(this).val();
    if (inputValue == "") {
      $(this).removeClass("filled");
      $(this).parents(".form-group").removeClass("focused");
    } else {
      $(this).addClass("filled");
    }
  });
  $("[required]")
    .closest(".contact_forms .form-group")
    .find(".form-label")
    .append($('<span class="required"> *</span>'));
});
$(".js-select2").select2({
  closeOnSelect: false,
  placeholder: "Choose years",
  allowHtml: false,
  allowClear: false,
  dropdownPosition: "below",
  tags: true,
});

$(".select_etab").select2({
  dropdownCssClass: "etablissement_list",
  minimumResultsForSearch: -1,
});

$(".select2-search__field").attr("readonly", "readonly");

$(function () {
  $(".test").click(function () {
    $(".globNav").hide();
    // $(".footer").hide();
    $(".show_gallery").fadeIn();
    $("body").addClass("no_scroll");
    let index_swip = $(this).data("index");
    var thumbs = new Swiper(".gallery-thumbs", {
      slidesPerView: "auto",
      spaceBetween: 10,
      centeredSlides: true,
      loop: true,
      slideToClickedSlide: true,
      initialSlide: index_swip,
    });
    var slider = new Swiper(".gallery-slider", {
      slidesPerView: 1,
      // centeredSlides: true,
      loop: true,
      loopedSlides: 6,
      initialSlide: index_swip,
      // simulateTouch: false,
      // noSwiping: true,
      navigation: {
        nextEl: ".swiper-gal-next",
        prevEl: ".swiper-gal-prev",
      },

      on: {
        slideChangeTransitionStart: function () {
          // Slide captions
          var swiper = this;
          var slideTitle = $(swiper.slides[swiper.activeIndex]).attr(
            "data-title"
          );
          var slideSubtitle = $(swiper.slides[swiper.activeIndex]).attr(
            "data-subtitle"
          );
          var slide_urllink = $(swiper.slides[swiper.activeIndex]).attr(
            "data-urllink"
          );
          $(".slide-captions_gal_title").html(function () {
            return "<h2 class='current-title'>" + slideTitle + "</h2>";
          });
          $(".slide-captions_gal_link").html(function () {
            return (
              '<a href="" class="link_share"> <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.17076 1.5C4.79413 1.5 3 3.78225 3 6.27267C3 7.42776 3.61087 8.86717 4.58971 9.32413C4.86869 9.45614 4.83261 9.29367 5.07311 8.32644C5.09235 8.2452 5.08273 8.17666 5.0202 8.1005C3.62049 6.39199 4.74603 2.88103 7.97114 2.88103C12.6368 2.88103 11.7638 9.69477 8.78163 9.69477C8.01203 9.69477 7.43963 9.05757 7.62241 8.27059C7.84127 7.33129 8.27177 6.32344 8.27177 5.64562C8.27177 3.93965 5.86436 4.19351 5.86436 6.45291C5.86436 7.15104 6.09764 7.62323 6.09764 7.62323C6.09764 7.62323 5.32323 10.9235 5.17893 11.5404C4.93603 12.5838 5.2126 14.2745 5.23665 14.4217C5.25108 14.503 5.33766 14.5284 5.38336 14.4624C5.46032 14.3557 6.39586 12.9468 6.65801 11.9288C6.75421 11.5581 7.14382 10.0527 7.14382 10.0527C7.40115 10.5452 8.14671 10.9565 8.93795 10.9565C11.2949 10.9565 13 8.76817 13 6.05435C12.9952 3.44715 10.886 1.5 8.17076 1.5Z" fill="white"/></svg> <label class="link_share_pr">' +
              slide_urllink +
              "</label> </a>"
            );
          });
          $(".slide-captions_gal_subtitle").html(function () {
            return "<h4 class='current-subtitle'>" + slideSubtitle + "</h4>";
          });
        },
      },
    });
    slider.controller.control = thumbs;
    thumbs.controller.control = slider;
  });
  $(".close_gal, .overlay").click(function () {
    $(".globNav").show();
    // $(".footer").show();
    $(".show_gallery").fadeOut();
    $("body").removeClass("no_scroll");
  });
  var Swipes = new Swiper(".swiper_full_width", {
    loop: false,
    slidesPerView: 2.7,
    // slidesPerColumn: 2,
    spaceBetween: 56,

    breakpoints: {
      1650: {
        slidesPerView: 2.7,
        spaceBetween: 56,
      },
      1024: {
        slidesPerView: 2.2,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 2.3,
        spaceBetween: 16,
      },
      700: {
        slidesPerView: 1.4,
      },

      500: {
        slidesPerView: 1.2,
      },

      400: {
        slidesPerView: 1.2,

        spaceBetween: 16,
      },

      375: {
        slidesPerView: 1.2,
        spaceBetween: 16,
      },
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });
  var Swipes = new Swiper(".swiper_news", {
    autoplay: {
      delay: 4000,
      disableOnInteraction: false,
    },
    speed: 500,
    loop: true,
    // pagination: {
    //     el: ".swiper-pagination",
    //     type: "fraction"
    // },

    pagination: {
      el: ".swiper-pagination",
      type: "custom",
      clickable: false,
      renderCustom: function (swiper, current, total) {
        function numberAppend(d) {
          return d < 10 ? "0" + d.toString() : d.toString();
        }
        return `<span class="left_pag"> ${numberAppend(
          current
        )}</span>  <span class="right_pag">${numberAppend(total)}</span>`;
      },
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  var Swipes = new Swiper(".swiper_instagram", {
    loop: false,
    spaceBetween: 56,
    // slidesPerView: 2.7,
    centeredSlides: false,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
    },
    breakpoints: {
      700: {
        slidesPerView: 2.5,
      },

      500: {
        slidesPerView: 1.3,
      },

      400: {
        slidesPerView: 1.3,

        spaceBetween: 16,
      },

      375: {
        slidesPerView: 1.3,
        spaceBetween: 16,
      },
    },
  });

  var Swipes = new Swiper(".swiper_experience", {
    loop: false,
    spaceBetween: 12,
    centeredSlides: false,

    pagination: {
      el: ".swiper-pagination",
    },
    breakpoints: {
      700: {
        slidesPerView: 2.5,
      },

      500: {
        slidesPerView: 1.3,
      },

      400: {
        slidesPerView: 1.3,

        spaceBetween: 16,
      },

      375: {
        slidesPerView: 1.3,
        spaceBetween: 16,
      },
    },
  });
});
const removeUpload = () => {
  console.log("test remove");
  var file_to_res = $(".upload_pdf_input");
  file_to_res.val("");
  $(".msg_upload_file").fadeOut();
};

$(document).ready(function () {
  $(".upload_pdf_input").change(function () {
    var filename = readURL(this);
    $(this).parent().children(".msg_upload_profile").html(filename);
    if ($(this).val()) {
      $(this).parent().children("div.help-block").hide();
    } else {
      $(this).parent().children("div.help-block").show();
    }
    let this_upload = $(this);
    $(".remove_file").click(function () {
      this_upload.val("");
      this_upload.parent().find(".success_msg").fadeOut();
    });
    setTimeout(function () {
      $(".error_msg").fadeOut();
    }, 10000);
  });

  function readURL(input) {
    var url = input.value;
    var ext = url.substring(url.lastIndexOf(".") + 1).toLowerCase();
    if (input.files && input.files[0] && ext == "pdf") {
      var path = $(input).val();
      var filename = path.replace(/^.*\\/, "");
      return (
        '<div class="msg_upload_file success_msg"><div class="name_file_up"> <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.8 6H17.25C20.5637 6 23.25 8.68629 23.25 12V12C23.25 15.3137 20.5637 18 17.25 18H4.60714C2.4769 18 0.75 16.2731 0.75 14.1429V14.1429C0.75 12.0126 2.4769 10.2857 4.60714 10.2857H16.8214C17.8866 10.2857 18.75 11.1492 18.75 12.2143V12.2143C18.75 13.2794 17.8865 14.1429 16.8214 14.1429H4.8" stroke="black"/></svg> <span class="name_up_file">' +
        filename +
        '</span></div><button  class="remove_file" type="button"> <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.5 4.5L19.3492 19.3492" stroke="#2B2B2B"/><path d="M4.5 19.3492L19.3492 4.5" stroke="#2B2B2B"/></svg></button> </div>'
      );
    } else {
      $(input).val("");
      return (
        '<div class="msg_upload_file error_msg"> &#9888 Only PDF formats are allowed !'.fontcolor(
          "#a98760"
        ) + "</div>"
      );
    }
  }
});

$(document).ready(() => {
  const button = document.querySelectorAll("[data-button-svg]");

  button.forEach((elemt) => {
    const animationProps = {
      repeat: -1,
      yoyo: true,
      paused: true,
      duration: 2,
    };

    const child_svg = $(elemt).children("svg").children();
    // console.log(child_svg);
    const text1 = child_svg[0];
    const text2 = child_svg[1];
    const text3 = child_svg[2];
    const text4 = child_svg[3];

    const firstCircle = gsap.to(text1, {
      morphSVG: "#morph-circle-1",
      duration: 0.4,
      delay: 0,
    });
    const secondCircle = gsap.to(text2, {
      morphSVG: "#morph-circle-2",
      duration: 0.4,
      delay: 0,
    });
    const thirdCircle = gsap.to(text3, {
      morphSVG: "#morph-circle-3",
      duration: 0.4,
      delay: 0,
    });

    const fourCircle = gsap.to(text4, {
      morphSVG: "#morph-circle-4",
      duration: 0.4,
      delay: 0,
    });
    const firstAction = gsap.to("#simple-circle-1", {
      morphSVG: "#morph-circle-1",
      ...animationProps,
    });
    const secondAction = gsap.to(text2, {
      morphSVG: "#morph-circle-2",
      ...animationProps,
    });
    const thirdAction = gsap.to(text3, {
      morphSVG: "#morph-circle-3",
      ...animationProps,
    });

    const fourthAction = gsap.to(text4, {
      morphSVG: "#morph-circle-4",
      ...animationProps,
    });

    const onCompleteSVGAnimation = () => {
      firstCircle.reverse();
      secondCircle.reverse();
      thirdCircle.reverse();
      fourCircle.reverse();
      // thirdCircle.reverse().then(() => (coverButton.style.pointerEvents = "all"));
    };
    elemt.addEventListener("mouseover", () => {
      firstAction.play();
      secondAction.play();
      thirdAction.play();
      fourthAction.play();
    });

    elemt.addEventListener("mouseleave", () => {
      firstAction.iteration(1);
      secondAction.iteration(1);
      thirdAction.iteration(1);
      fourthAction.iteration(1);
      firstAction.reverse();
      secondAction.reverse();
      thirdAction.reverse();
      fourthAction.reverse();
    });

    gsap.fromTo(
      elemt,
      { opacity: 0 },
      {
        opacity: 1,
        duration: 0.8,
        delay: 5,
        onComplete: onCompleteSVGAnimation,
      }
    );
    elemt.addEventListener("mouseover", () => {
      firstAction.play();
      secondAction.play();
      thirdAction.play();
      fourthAction.play();
    });

    elemt.addEventListener("mouseleave", () => {
      firstAction.iteration(1);
      secondAction.iteration(1);
      thirdAction.iteration(1);
      fourthAction.iteration(1);
      firstAction.reverse();
      secondAction.reverse();
      thirdAction.reverse();
      fourthAction.reverse();
    });
  });
});
